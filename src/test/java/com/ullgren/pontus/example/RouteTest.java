package com.ullgren.pontus.example;

import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;

import org.junit.Test;

public class RouteTest extends CamelBlueprintTestSupport {
	
    @Override
    protected String getBlueprintDescriptor() {
        return "/OSGI-INF/blueprint/blueprint.xml";
    }

    @Test
    public void testRoute() throws Exception {
    	sendBody("direct:start", getClass().getClassLoader().getResourceAsStream("orders.csv"));
        getMockEndpoint("mock:result").expectedMinimumMessageCount(3);

        // assert expectations
        assertMockEndpointsSatisfied();
    }

}
