package com.ullgren.pontus.example.model;

import org.apache.camel.dataformat.bindy.annotation.*;
import java.io.Serializable;

@CsvRecord(separator = ",", skipFirstLine = true)
public class Order implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@DataField(pos = 1)
	private String id;

	@DataField(pos = 2)
	private String customerName;

	@DataField(pos = 3)
	private String customerEmail;

	@DataField(pos = 4)
	private String productNo;

	@DataField(pos = 5)
	private String amount;

	private boolean dispatched;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getProductNo() {
		return productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public boolean isDispatched() {
		return dispatched;
	}

	public void setDispatched(boolean dispatched) {
		this.dispatched = dispatched;
	}

}
